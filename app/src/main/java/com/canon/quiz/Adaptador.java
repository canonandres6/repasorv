package com.canon.quiz;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;


public class Adaptador extends RecyclerView.Adapter <Elemento> {

    List<String> Elementos;

    public  Adaptador(List<String> datos){

        Elementos=datos;
    }



    @NonNull
    @Override
    public Elemento onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View miCajon;
        miCajon = LayoutInflater.from(parent.getContext()).inflate(R.layout.elemento,parent, false);
        return new Elemento(miCajon);



    }

    @Override
    public void onBindViewHolder(@NonNull Elemento holder, int position) {

        String miDato = Elementos.get(position);
        holder.tvElemento.setText(miDato);

    }

    @Override
    public int getItemCount() {
     if (Elementos!= null){
         return Elementos.size();
     }
     else {
         return 0;
     }
    }
}
