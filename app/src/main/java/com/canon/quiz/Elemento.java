package com.canon.quiz;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class Elemento extends RecyclerView.ViewHolder{

    TextView tvElemento;

    public Elemento(@NonNull View itemView){
        super(itemView);

        tvElemento = itemView.findViewById(R.id.tvElemento);
    }

}
