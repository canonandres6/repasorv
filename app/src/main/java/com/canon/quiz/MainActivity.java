package com.canon.quiz;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView rvElementos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvElementos.findViewById(R.id.rvElementos);
        rvElementos.setLayoutManager(new LinearLayoutManager((this)));

        List<String> misDatosLocales = new ArrayList<>();

        misDatosLocales.add("Andres");
        misDatosLocales.add("Carlos");
        misDatosLocales.add("Pedro");
        misDatosLocales.add("Luis");

        Adaptador miAdaptador = new Adaptador(misDatosLocales);

        rvElementos.setAdapter(miAdaptador);

        getElementos();

    }
    public void getElementos(){
        String miUrl = "https://www.balldontlie.io/api/v1/teams";

        RequestQueue miPila = Volley.newRequestQueue(this);

        JsonObjectRequest miPeticion;

        miPeticion = new JsonObjectRequest(Request.Method.GET, miUrl, null,

                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response){
                        try {

                            JSONArray miArreglo = response.getJSONArray("data");
                            List<String> elementos = new ArrayList<>();
                            
                            for(int i =0;i<miArreglo.length();i++){
                                
                                JSONObject miElemento= miArreglo.getJSONObject(i);
                                elementos.add(miElemento.getString("fil_name"));
                            }
                            RecyclerView rv;
                            rv= findViewById(R.id.rvElementos);
                            Adaptador miAdaptador = new Adaptador(elementos);
                                    
                            rv.setAdapter(miAdaptador);
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },new Response.ErrorListener(){

            @Override
            public void onError
        }

        )


    }
}
